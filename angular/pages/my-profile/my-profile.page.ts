import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActionSheetController, ToastController, Platform, ModalController, AlertController } from '@ionic/angular';
import { WeeklitAPIProvider } from '../../services/weeklit-api/weeklit-api';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { AfAuthProvider } from '../../services/af-auth/af-auth';
import { Router } from '@angular/router';
import { CropPage } from '../crop/crop.page';
import { Location } from '@angular/common';
import { Facebook } from '@ionic-native/facebook/ngx';
import { Firebase } from '@ionic-native/firebase/ngx';
import { HtmlHeaderService } from 'src/app/services/html-header/html-header.service';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.page.html',
  styleUrls: ['./my-profile.page.scss'],
})
export class MyProfilePage implements OnInit, AfterViewInit {

  userInfos
  userToDisplay
  editUserForm: FormGroup;
  image
  imageUploaded = false
  user = {id :"", firstName  : "", lastName: "", email : "", userName: "", description: "", profilePicture: ""};
  disableButton = false
  profilePicture
  isLoggedIn: boolean;
  credentials: { uid: any; token: any; };

  constructor(public router: Router, private weeklitAPI : WeeklitAPIProvider, private camera : Camera, private actionSheetCtrl : ActionSheetController, private platform : Platform, private toast : ToastController, private afAuth: AfAuthProvider, private modal : ModalController, private location:Location, private alert : AlertController, private facebook : Facebook, private firebase : Firebase, private headers : HtmlHeaderService) { }

  ngOnInit() {
    let EMAILPATTERN = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9- ]*[a-z0-9 ])?)*$/i;
    this.editUserForm = new FormGroup({
      firstName: new FormControl('', [Validators.required, Validators.pattern('[a-zA-ZÀ-ÿ ]*'), Validators.minLength(3), Validators.maxLength(30)]),
      userName: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z0-9_.-]*$'), Validators.minLength(3), Validators.maxLength(30)]),
      lastName: new FormControl('', [Validators.required, Validators.pattern('[a-zA-ZÀ-ÿ ]*'), Validators.minLength(3), Validators.maxLength(30)]),
      email: new FormControl('', [Validators.required, Validators.pattern(EMAILPATTERN)]),
    });
  }

  ngAfterViewInit(){
    this.checkSession()
  }

  async checkSession(){    
    
    var isLoggedIn = await this.afAuth.authenticated().valueOf();
    isLoggedIn = isLoggedIn.valueOf()

    if (isLoggedIn != false) {
      await this.afAuth.getAuthState().subscribe(user => {
        let uid = user.uid
        this.getUserInfosAfterCheckSession(uid)
      });
    }
  }

  ionViewWillEnter(){
    this.headers.updateMetas("Modifier mon profil - weeklit", 'Modifiez les informations de votre profil weeklit.')
    this.headers.noIndex(true)
  }


  async getUserInfosAfterCheckSession(uid){
    const token = await this.afAuth.getIdToken()
    this.credentials = {uid : uid, token : token}
    const result:any = await this.weeklitAPI.getUserByUid(this.credentials.uid)
    if (!result.error) {
      this.userToDisplay = result.user
      this.user = this.userToDisplay
    } else {
      const toast = await this.toast.create({
        message : 'Impossible de récupérer les informations du profil',
        duration : 3000
      })
      toast.present()
    }
  }

  async useCamera() {
    let actionSheet = await this.actionSheetCtrl.create({
      header: 'Sélectionnez la source',
      buttons: [
        {
          text: 'appareil photo',
          icon: 'camera',
          handler: () => {
            if (this.platform.is('cordova')) {
              this.takePicture(this.camera.PictureSourceType.CAMERA);
            } else {
              this.weeklitAPI.displayNativeAppAlert()
            }
          }
        },
        {
          text: 'album',
          icon: 'albums',
          handler: () => {
            if (this.platform.is('cordova')) {
              this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
            } else {
              this.weeklitAPI.displayNativeAppAlert()
            }
          }
        },
        {
          text: 'Annuler',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  async takePicture(sourceType){
    const options: CameraOptions = {
      targetWidth: 1200,
      targetHeight: 1200,
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType : sourceType,
      correctOrientation: true

    }
      
    const fileUri = await this.camera.getPicture(options)
    let base64Image:any = 'data:image/jpeg;base64,' + fileUri;
    const modal = await this.modal.create({
      component: CropPage,
      componentProps: {
        'image': base64Image,
        'aspectRatio': 1
      }
    });
    await modal.present()
    const { data } = await modal.onDidDismiss();
    this.user.profilePicture = data.croppedImage
    this.imageUploaded = true
  }


    async updateUser(){
      this.disableButton = true
    try{
      if (this.user.profilePicture && this.imageUploaded == true) {

        const result:any = await this.weeklitAPI.uploadPhoto(this.user.profilePicture, this.user.userName)

        if (result.error) {
          var error;
      
          if (result.message) {
            error = result.message
          } else {
            error = result.error
          }
          throw new Error("Erreur lors de l'upload de la photo  : " + error);
        } 
        else{
          this.user.profilePicture = result.file.url
          this.imageUploaded = false
        }
      }

      

      const result:any = await this.weeklitAPI.updateUser(this.user)
      if (!result.error) {
        this.facebook.logEvent('user updated', {id: this.user.id})
        this.firebase.logEvent('user updated', {id: this.user.id})
        let toast = await this.toast.create({
          message : 'la mise à jour de votre profil a bien été effectuée.',
          duration : 3000
        })
        toast.present()
        
        this.location.back()

      } else{
        throw new Error("Erreur lors de la mise à jour de l'utilisateur  : " + result.error.text);
      }


      this.disableButton = false
    } catch (error) {
      this.firebase.logError("Erreur lors de la mise à jour de l'utilisateur  : " + error.message)
      this.disableButton = false
      let toast = await this.toast.create({
        message : error.message,
        duration : 3000
      })
      toast.present();

    }   
  }







}
