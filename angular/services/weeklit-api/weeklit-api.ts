import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { CONFIG } from '../../config/config';
import { AfAuthProvider } from '../af-auth/af-auth';
import { Firebase } from '@ionic-native/firebase/ngx';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class WeeklitAPIProvider {

  API_BASE_URL ;
  data;
  conf = CONFIG;
  password = 'app-password';
  username = 'app-account';
  httpAuth = window.btoa(this.username+':'+this.password);
  query;
  sessionId : string;
  response;
  userId : number; 
  callback: boolean;
  token ;
  authHeader = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded').set('Authorization', 'basic '+ this.httpAuth); 
  uid: any;
  result
  idToken
  cookies: any;

  constructor(private http : HttpClient, private storage: Storage, private firebase: Firebase, private afAuth : AfAuthProvider, private afMessaging : AngularFireMessaging, private auth : AngularFireAuth) {
    this.API_BASE_URL = this.conf.apiUrl
    this.getCredentials()
    this.afAuth.afAuth.authState.subscribe(user => {
      if (user) {
        this.uid = user.uid
      }
    })

    this.afAuth.afAuth.auth.onIdTokenChanged(token => {
      this.result = token
      if(this.result){
        this.setToken()
      }
    })
  }

  async postRequest(url, query, authRequired ?){
    if (authRequired) {
      if (!this.token) {
        await this.setToken()
      }
      query['uid'] = this.uid
      query['token'] = this.token
    }
    return new Promise(resolve => {
      this.http.post(this.API_BASE_URL + url, query, {headers: this.authHeader}).pipe(map(res => res)).subscribe(data =>{this.response = data;          
        resolve(this.response);},
      err =>{this.response = err;         
        resolve(this.response);
      });
    });
  }

  async getRequest(url){
    return new Promise(resolve => {
      this.http.get(this.API_BASE_URL + url, {headers: this.authHeader}).pipe(map(res => res)).subscribe(data =>{this.response = data;          
        resolve(this.response);},
      err =>{this.response = err;         
        resolve(this.response);
      });
    });
  }

  async setToken(){
    const token = await this.afAuth.getIdToken()
    this.token = token
    if (!this.uid) {
      this.afAuth.afAuth.authState.subscribe(user => {
        const userInfos = user;
        this.result = userInfos
        this.uid = userInfos.uid
      })
    }
  }
  async getCredentials(){

    let token : any = await this.afAuth.getIdToken()
    if (token) {
      this.token = token
    }
    this.afAuth.afAuth.authState.subscribe(user => {
    const userInfos = user;
    
      if (user) {
        this.result = userInfos
        this.uid = userInfos.uid
        this.setToken();
        this.initUserId(this.uid)
      }
    })
  }


  async initUserId(uid){
    const user = await this.getUserByUid(uid)
    this.result = user
    if (this.result.user) {
      this.userId = this.result.user.id
    }
  }

  
  async generatePasswordResetToken(email){
    let query = { "email" : email };
    return await this.postRequest('/user/generate-password-refresh-token.php', query)
  }

  
  async updateUser(user){
        this.query = user
        this.query.userId = this.userId
        this.query.sessionId = this.sessionId
    return await this.postRequest('/user/update-user-infos.php', this.query, true)
  }
  
  async uploadPhoto(photo:string, recipeName:string){
    let query = { "photo" : photo, "recipeName" : recipeName, "userId" : this.userId };
    return await this.postRequest('/photo/upload.php', query, true)
  }

  async searchIngredient(name : string){
    let query = { "string" : name };
    return await this.postRequest('/ingredient/search.php', query)
  }

  async getRecipeById(id : number){
    let query = { "id" : id };
    return await this.postRequest('/recipe/get-by-id.php', query)
  }

  async getNotificationById(id){
    let query = { "id" : id };
    return await this.postRequest('/notification/get-by-id.php', query)
  }

  async createPlanningItem(planningId, date, meal, portions, dish){
    let query = { 
      "planningId" : planningId,
      "date" : date,
      "meal" : meal,
      "portions" : portions,  
      "dish" : dish,  
    };
    return await this.postRequest('/planning-item/create.php', query, true)
  }





}
