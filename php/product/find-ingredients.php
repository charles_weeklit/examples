<?php

include_once realpath(dirname(__FILE__).'/../config/database.php');
include_once realpath(dirname(__FILE__).'/../objects/product.php');

$database = new Database();
$db = $database->getConnection();
$data = json_decode(file_get_contents('php://input'), true);

try{
    if(isset($data["listId"])){
        $listId = $data["listId"];
        $product = new Product($db);
        if (isset($data["options"])) {
            $options = $data["options"];
            $result = $product->findIngredients($listId, $options);
        } else {
            $result = $product->findIngredients($listId);
        }
        echo json_encode($result);
    }
    else {
        throw new Exception('Merci de bien vouloir renseigner les infos nécessaire pour product/find-ingredients.');
        return false;
    }
}
catch(Exception $e){
    echo json_encode(array(
        'error' => $e->getMessage(),
        'errorCode' => $e->getCode()
    ));
}

?>