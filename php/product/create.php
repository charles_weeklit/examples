<?php
include_once realpath(dirname(__FILE__).'/../config/database.php');
include_once realpath(dirname(__FILE__).'/../objects/product.php');
include_once realpath(dirname(__FILE__).'/../objects/user.php');

$database = new Database();
$db = $database->getConnection();

$data = json_decode(file_get_contents('php://input'), true);

try{
    if(isset($data["product"])){
        $productData = $data["product"];
        $product = new Product($db);
        $result = $product->create($productData);
        echo json_encode($result);
    }
    else {
        throw new Exception('Merci de bien vouloir renseigner les infos nécessaire pour product/create.');
        return false;
    }

}
catch(Exception $e){
    echo json_encode(array(
        'error' => $e->getMessage(),
        'errorCode' => $e->getCode()
    ));
}




?>