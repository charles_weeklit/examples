<?php
include_once realpath(dirname(__FILE__).'/../objects/auth.php');
include_once realpath(dirname(__FILE__).'/../objects/user.php');
include_once realpath(dirname(__FILE__).'/../objects/db-manager.php');
include_once realpath(dirname(__FILE__).'/../config/config.php');

class Product{
 
    private $conn;
    private $auth;
    private $token;
    private $user;
    private $table_name = "products";

    public function __construct($db, $user = NULL, $token = NULL){
        $this->conn = $db;
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->user = $user;
        $this->token = $token;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// creates a new product in DB
    function create($product){
        try {
            if (isset($product['ingredientId']) && isset($product['productId']) && isset($product['shopName']) && isset($product['location']) && isset($product['available']) && isset($product['price']) && isset($product['priceReference']) && isset($product['title']) && isset($product['conditioning'])) {
                $product['active'] = true;
                $DBM = new DBManager($this->conn, $this->table_name);
                $response = $DBM->create($product);
                return $response;
            } else {
                throw new Exception("missing object propertie for product create", 201);
            }
        } catch(Exception $e){
            $errCode = (is_int($e->getCode())) ? $e->getCode() : intval('22'.preg_replace('/[^0-9.]+/', '', $e->getCode())) ;
            throw new Exception('error in Product::create : '.$e->getMessage(), $errCode);        
        }
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// gets a products depending on options
    function get($options){
        try {
            $DBM = new DBManager($this->conn, $this->table_name);
            $result= $DBM->get($options);
            $response['message'] = ($result['hits'] > 0) ? 'La liste des produits a bien été retrouvée' : 'Pas de produit pour ces critères' ;
            $response['hits']= $result['hits'];
            $response['products']= $result['items'];
            return $response;
        } catch(Exception $e){
            $errCode = (is_int($e->getCode())) ? $e->getCode() : intval('22'.preg_replace('/[^0-9.]+/', '', $e->getCode())) ;
            throw new Exception('error in Product::getBy : '.$e->getMessage(), $errCode);    
        }
    }


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// update a given product
    function update($product){
        try {
            $DBM = new DBManager($this->conn, $this->table_name);
            $response['product'] = $DBM->update($product);
            $response['message'] = 'le produit a bien été mis à jour';
            return $response;
        } catch(Exception $e){
            $errCode = (is_int($e->getCode())) ? $e->getCode() : intval('22'.preg_replace('/[^0-9.]+/', '', $e->getCode())) ;
            throw new Exception('error in Product::update : '.$e->getMessage(), $errCode);    
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// UGLY CODE NEEDED TO PARSE SCRAPED DATA
function getQuantityNeeded($listItem, $product){
    try {

            $neededQuantity = $listItem['quantity'];
            if (isset($product['conditioning']['unit'])) {
              if ($listItem['unit'] == str_replace(" ", "", $product['conditioning']['unit'])) {
                if (isset($product['conditioning']['multiplicator'])) {
                  $numberOfProducts = ceil($neededQuantity/($product['conditioning']['quantity']*$product['conditioning']['multiplicator']));
                } else {
                  $numberOfProducts = ceil($neededQuantity/$product['conditioning']['quantity']);
                }
              }else {
                $numberOfProducts = ceil($neededQuantity/$product['conditioning']['quantity']);
              }
            } else if (isset($product['conditioning']['g'])){
              if (str_replace(" ", "", $listItem['unit']) == str_replace(" ", "", $product['conditioning']['g']['unit'])){
                $numberOfProducts = ceil($neededQuantity/$product['conditioning']['g']['quantity']);
              } else {
                $numberOfProducts = ceil($neededQuantity/$product['conditioning']['g']['quantity']);
              }
            } else if ($listItem['unit'] == "" && isset($listItem['quantity']) && isset($product['conditioning']['unit']) && $product['conditioning']['unit'] == ""){
              $numberOfProducts = ceil($neededQuantity/$product['conditioning']['quantity']);
            } else {
              $numberOfProducts = 30;
            }

            $productPrice = $numberOfProducts * $product['price'];
        
            return array(
                "tmp_selectNumber" => $numberOfProducts,
                "tmp_productPrice" => $productPrice
            );

    } catch(Exception $e){
        $errCode = (is_int($e->getCode())) ? $e->getCode() : intval('22'.preg_replace('/[^0-9.]+/', '', $e->getCode())) ;
        throw new Exception('error in Product::update : '.$e->getMessage(), $errCode);    
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// finds every products that could match for every shopping list item
    function findIngredients($listId, $options = array()){
        try {
            // on récupère la liste de courses
            include_once realpath(dirname(__FILE__).'/../objects/shoppingList.php');
            $shoppingList = new ShoppingList($this->conn);
            $list = $shoppingList->compile($listId)['shoppingList'];
            $ingredientIdArray = [];

            // on ne garde que les ID des ingrédients
            foreach ($list as $key => $item) {
                array_push($ingredientIdArray, $item['ingredient']['id']);
            }

            $results = array();

            foreach ($ingredientIdArray as $key => $ingredientId) {
                $search = $options;
                array_push($search, array("selector" => "ingredientId" ,"value" => $ingredientId));
                // on recherche les produits correspondants aux ingrédients
                $response = $this->getBy($search);{
                foreach ($response['products'] as $key2 => $product) {
                    $product['price'] = floatval($product['price']);
                    if ($list[$key]['quantity'] == 0) {
                      $list[$key]['quantity'] = 1;
                    } 
                    // pour chaque produit, on calcule combien ça couterait
                    $res = $this->getQuantityNeeded($list[$key], $product);
                    $product['tmp_selectNumber'] = $res['tmp_selectNumber'];
                    $product['tmp_productPrice'] = $res['tmp_productPrice'];
                    similar_text($list[$key]['ingredient']['name'], $product['title'], $product['similarity']);
                    $response['products'][$key2] = $product;
                }
                if (is_array($response['products'])) {
                    // tri en fonction du prix
                    usort($response['products'], function($a, $b) {
                        if ($a['tmp_productPrice'] < $b['tmp_productPrice']){ return -1; } 
                        else if ($a['tmp_productPrice'] > $b['tmp_productPrice']){ return 1; }
                        else {return 0;};
                    });
                    // tri en fonction de la similarité des textes (entre nom de l'ingrédient et nom du produit)
                    usort($response['products'], function($a, $b) {
                        if (round($a['similarity']*10)/10 > round($b['similarity']*10)/10 ){ return -1; } 
                        else if (round($a['similarity']*10)/10  < round($b['similarity']*10)/10 ){ return 1; }
                        else {return 0;};
                    });

                    // attribution d'un score plus précis en fonction des choix de produits des autres utilisateurs
                    $scores = $this->useEventsDataInIngredientSearch($ingredientId);
                    foreach ($response['products'] as $productKey => $product) {
                        if (isset($scores[$response['products'][$productKey]['productId']])) {
                            $response['products'][$productKey]['score'] = $scores[$response['products'][$productKey]['productId']];
                        } else {
                            $response['products'][$productKey]['score'] = 0;
                        }
                    }
                    usort($response['products'], function($a, $b) {
                        if ($a['score'] > $b['score']){ return -1; } 
                        else if ($a['score'] < $b['score']){ return 1; }
                        else {return 0;};
                    });
                }
                $res = [];
                $res['hits'] = intval($response['hits']);
                $res['ingredientId'] = $list[$key]['ingredient']['id'];
                $res['products'] = $response['products'];
                array_push($results, $res);
            }
            $response = [];
            $response['message'] = 'products found';
            $response['results'] = $results;

            return $response;
        }
        } catch(Exception $e){
            $errCode = (is_int($e->getCode())) ? $e->getCode() : intval('22'.preg_replace('/[^0-9.]+/', '', $e->getCode())) ;
            throw new Exception('error in Product::findIngredients : '.$e->getMessage(), $errCode);    
        }
    }



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// update
    function useEventsDataInIngredientSearch($ingredientId){
        try {
            include_once realpath(dirname(__FILE__).'/../objects/product-event.php');
            include_once realpath(dirname(__FILE__).'/../objects/ingredient.php');
            $productEvent = new ProductEvent($this->conn);
            $ingredient = new Ingredient($this->conn);
            $ingredient = $ingredient->getById($ingredientId);

            $options = [
                array(
                    "selector" => "ingredientId",
                    "value" => $ingredient['id']
                )
            ];

            $res = null;    
            $res = $productEvent->getBy($options);

            $scores = array();

            if ($res['hits'] > 0) {
                foreach ($res['events'] as $key2 => $event) {
            
                    if ($event['event'] == "changeSelection" || $event['event'] == "searchAndSelect") {
                        if (isset($scores[$event['newProductId']])) {
                            $scores[$event['newProductId']] = $scores[$event['newProductId']] +1;
                        } else {
                            $scores[$event['newProductId']] = 1;
                        }
                    } else if ($event['event'] == "selectedPropProduct") {
                        if (isset($scores[$event['productPropId']])) {
                            $scores[$event['productPropId']] = $scores[$event['productPropId']] +1;
                        } else {
                            $scores[$event['productPropId']] = 1;
                        }
                    }
                }
            }
            if (empty($scores)) {
                return null;
            } else {
                return $scores;
            }
            
        } catch(Exception $e){
            $errCode = (is_int($e->getCode())) ? $e->getCode() : intval('22'.preg_replace('/[^0-9.]+/', '', $e->getCode())) ;
            throw new Exception('error in Product::update : '.$e->getMessage(), $errCode);    
        }
    }

}