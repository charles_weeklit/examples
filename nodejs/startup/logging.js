require('express-async-errors')
const winston = require('winston')
// require('winston-mongodb')

module.exports = function () {
    process.on('unhandledRejection', (ex) => {
        throw ex
    })
    
    winston.exceptions.handle(
        new winston.transports.File({filename : 'uncaughtExceptions.log'}),
        new winston.transports.Console({colorize : true, prettyPrint : true}),
        // new winston.transports.MongoDB({db : 'mongodb://localhost/vidly', collection : 'uncaughtErrorsLogs'})
    )
    winston.add(
        new winston.transports.File({filename : 'logfile.log'}),
        new winston.transports.Console({colorize : true, prettyPrint : true}),
        // new winston.transports.MongoDB({db : 'mongodb://localhost/vidly', level : 'info'})
    )
}
