const Joi = require('joi');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken')
const config = require('config')

const userSchema = new mongoose.Schema({
  name : {
      type : String,
      minlength : 3,
      maxlength : 200,
      trim : true
  },
  email : {
      type : String,
      minlength : 6,
      maxlength : 200,
      trim : true,
      unique : true
  },
  password : {
      type : String,
      minlength : 6,
      maxlength : 200,
      trim : true
  },
  isAdmin : {
    type : Boolean,
  }
})

userSchema.methods.generateAuthToken = function(){
  const token = jwt.sign({ _id : this._id, isAdmin : this.isAdmin}, config.get('jwtPrivateKey'))
  return token
}

const User = mongoose.model('user', userSchema);

  function validateUser(user) {
    const schema = {
      name: Joi.string().min(3).max(200).required(),
      email: Joi.string().min(6).max(200).required().email(),
      password: Joi.string().min(6).max(200).required(),
    };
  
    return Joi.validate(user, schema);
  }
  
  exports.User = User; 
  exports.validate = validateUser;