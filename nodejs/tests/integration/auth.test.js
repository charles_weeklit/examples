const {User} = require('../../models/user');
const {Company} = require('../../models/company');
const request = require('supertest');

describe('auth middleware', () => {
  beforeEach(() => { server = require('../../index'); })
  afterEach(async () => { 
    await Company.remove({});
    await User.remove({});
    server.close(); 
  });

  let token; 
  let userId

  const exec = () => {
    return request(server)
      .post('/companies')
      .set('x-auth-token', token)
      .send({ name: 'company1', user : userId });
  }

  beforeEach( async () => {
    token = new User().generateAuthToken();

    user =  await new User({ name : "carlos", email : "charles@weekl.it", password : "123456789" }).save()
    userId = user._id
  });

  it('should return 401 if no token is provided', async () => {
    token = ''; 

    const res = await exec();

    expect(res.status).toBe(401);
  });

  it('should return 400 if token is invalid', async () => {
    token = 'a'; 

    const res = await exec();

    expect(res.status).toBe(400);
  });

  it('should return 200 if token is valid', async () => {
    const res = await exec();

    expect(res.status).toBe(200);
  });
});