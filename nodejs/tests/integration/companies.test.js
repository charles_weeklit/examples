const request = require('supertest');
const {Company} = require('../../models/company');
const {User} = require('../../models/user');
const mongoose = require('mongoose');

let server;

describe('/companies', () => {
  beforeEach(() => { server = require('../../index'); })
  afterEach(async () => { 
    server.close(); 
    await Company.remove({});
    await User.remove({});
  });

  describe('GET /', () => {
    it('should return all companies', async () => {
      const companies = [
        { name: 'company1' },
        { name: 'company2' },
      ];
      
      await Company.collection.insertMany(companies);

      const res = await request(server).get('/companies');
      
      expect(res.status).toBe(200);
      expect(res.body.length).toBe(2);
      expect(res.body.some(g => g.name === 'company1')).toBeTruthy();
      expect(res.body.some(g => g.name === 'company2')).toBeTruthy();
    });
  });

  describe('GET /:id', () => {
    it('should return a company if valid id is passed', async () => {
      const company = new Company({ name: 'company1' });
      await company.save();

      const res = await request(server).get('/companies/' + company._id);

      expect(res.status).toBe(200);
      expect(res.body).toHaveProperty('name', company.name);     
      expect(res.body).toHaveProperty('treasury');     
    });

    it('should return 404 if invalid id is passed', async () => {
      const res = await request(server).get('/companies/1');

      expect(res.status).toBe(404);
    });

    it('should return 404 if no company with the given id exists', async () => {
      const id = mongoose.Types.ObjectId();
      const res = await request(server).get('/companies/' + id);

      expect(res.status).toBe(404);
    });
  });

  describe('POST /', () => {

    // Define the happy path, and then in each test, we change 
    // one parameter that clearly aligns with the name of the 
    // test. 
    let token; 
    let name; 
    let userId

    const exec = async () => {
      return await request(server)
        .post('/companies')
        .set('x-auth-token', token)
        .send({ name , user : userId });
    }

    beforeEach(() => {
      token = new User().generateAuthToken();      
      name = 'company1'; 
    })

    it('should return 401 if client is not logged in', async () => {
      token = ''; 
      user =  await new User({ name : "carlos", email : "charles@weekl.it", password : "123456789" }).save()
      userId = user._id

      const res = await exec();

      expect(res.status).toBe(401);
    });

    it('should return 400 if company name is less than 3 characters', async () => {
      name = '12'; 
      user =  await new User({ name : "carlos", email : "charles@weekl.it", password : "123456789" }).save()
      userId = user._id
      
      const res = await exec();

      expect(res.status).toBe(400);
    });

    it('should return 400 if company name is more than 50 characters', async () => {
      name = new Array(52).join('a');
      user =  await new User({ name : "carlos", email : "charles@weekl.it", password : "123456789" }).save()
      userId = user._id

      const res = await exec();

      expect(res.status).toBe(400);
    });

    it('should return 400 if company name is already used', async () => {
      name = "company1";
      user =  await new User({ name : "carlos", email : "charles@weekl.it", password : "123456789" }).save()
      userId = user._id
      await new Company({name : 'company1', user : userId}).save()

      const res = await exec();

      expect(res.status).toBe(400);
    });

    it('should return 400 if the user doesn\'t exists', async () => {
      name = 'company1';
      userId = user._id

      const res = await exec();

      expect(res.status).toBe(400);
    });

    it('should save the company if it is valid', async () => {
      user =  await new User({ name : "carlos", email : "charles@weekl.it", password : "123456789" }).save()
      userId = user._id
      name = "company1"

      await exec();

      const company = await Company.find({ name: 'company1' });

      expect(company).not.toBeNull();
    });

    it('should return the company if it is valid', async () => {
      user =  await new User({ name : "carlos", email : "charles@weekl.it", password : "123456789" }).save()
      userId = user._id
      name = "company2"

      const res = await exec();

      expect(res.body).toHaveProperty('_id');
      expect(res.body).toHaveProperty('name', 'company2');
    });
  });

  describe('PUT /:id', () => {
    let token; 
    let newName; 
    let company; 
    let id; 
    let userId

    const exec = async () => {
      return await request(server)
        .put('/companies/' + id)
        .set('x-auth-token', token)
        .send({ name: newName, user : userId });
    }

    beforeEach(async () => {
      // Before each test we need to create a company and 
      // put it in the database.      
      company = new Company({ name: 'company1' });
      await company.save();

      user =  await new User({ name : "carlos", email : "charles@weekl.it", password : "123456789" }).save()
      userId = user._id
      
      token = new User().generateAuthToken();     
      id = company._id; 
      newName = 'updatedName'; 
    })

    it('should return 401 if client is not logged in', async () => {
      token = ''; 

      const res = await exec();

      expect(res.status).toBe(401);
    });

    it('should return 400 if company name is less than 3 characters', async () => {
      newName = '12'; 
      
      const res = await exec();

      expect(res.status).toBe(400);
    });

    it('should return 400 if company name is more than 50 characters', async () => {
      newName = new Array(52).join('a');

      const res = await exec();

      expect(res.status).toBe(400);
    });

    it('should return 404 if id is invalid', async () => {
      id = 1;

      const res = await exec();

      expect(res.status).toBe(404);
    });

    it('should return 404 if company with the given id was not found', async () => {
      id = mongoose.Types.ObjectId();

      const res = await exec();

      expect(res.status).toBe(404);
    });

    it('should return 400 if the user id is invalid', async () => {
      id = mongoose.Types.ObjectId();
      userId = ''

      const res = await exec();

      expect(res.status).toBe(400);
    });

    it('should return 400 if the user for the given user id doesn\'t exists', async () => {
      id = mongoose.Types.ObjectId();
      userId = mongoose.Types.ObjectId();

      const res = await exec();

      expect(res.status).toBe(400);
    });

    it('should update the company if input is valid', async () => {
      await exec();

      const updatedCompany = await Company.findById(company._id);

      expect(updatedCompany.name).toBe(newName);
    });

    it('should return the updated company if it is valid', async () => {
    
      const res = await exec();

      expect(res.body).toHaveProperty('_id');
      expect(res.body).toHaveProperty('name', newName);
    });
  });  

  describe('DELETE /:id', () => {
    let token; 
    let company; 
    let id; 

    const exec = async () => {
      return await request(server)
        .delete('/companies/' + id)
        .set('x-auth-token', token)
        .send();
    }

    beforeEach(async () => {
      // Before each test we need to create a company and 
      // put it in the database.      
      company = new Company({ name: 'company1' });
      await company.save();
      
      id = company._id; 
      token = new User({ isAdmin: true }).generateAuthToken();     
    })

    it('should return 401 if client is not logged in', async () => {
      token = ''; 

      const res = await exec();

      expect(res.status).toBe(401);
    });

    it('should return 403 if the user is not an admin', async () => {
      token = new User({ isAdmin: false }).generateAuthToken(); 

      const res = await exec();

      expect(res.status).toBe(403);
    });

    it('should return 404 if id is invalid', async () => {
      id = 1; 
      
      const res = await exec();

      expect(res.status).toBe(404);
    });

    it('should return 404 if no company with the given id was found', async () => {
      id = mongoose.Types.ObjectId();

      const res = await exec();

      expect(res.status).toBe(404);
    });

    it('should delete the company if input is valid', async () => {
      await exec();

      const companyInDb = await Company.findById(id);

      expect(companyInDb).toBeNull();
    });

    it('should return the removed company', async () => {
      const res = await exec();

      expect(res.body).toHaveProperty('_id', company._id.toHexString());
      expect(res.body).toHaveProperty('name', company.name);
    });
  });  
});